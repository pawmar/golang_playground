// FIFO int Queue with fixed size data container

package fqueue

import (
    "errors"
)

type Queue struct {
    head int;
    tail int;
    size int;
    max int;
    data []int;
}

func NewQueue(size int) *Queue {
    return &Queue{
        head: 0,
        tail: 0,
        size: 0,
        max: size,
        data: make([]int, size)}
}

func (q *Queue) Enqueue(val int) bool {
    if q.size < q.max {
        q.data[q.tail] = val
        q.tail += 1
        q.size += 1
        if q.tail == q.max {
            q.tail = 0
        }
        return true
    }
    return false
}

func (q *Queue) Dequeue() (int, error) {
    if q.size > 0 {
        val := q.data[q.head]
        q.head += 1
        q.size -= 1
        if q.head == q.max {
            q.head = 0
        }
        return val, nil
    }
    return 0, errors.New("Queue is empty")
}

func (q *Queue) Empty() bool {
    return q.size == 0
}

func (q *Queue) Full() bool {
    return q.size == q.max
}

func (q *Queue) Clear() {
    q.head = 0
    q.tail = 0
    q.size = 0
}