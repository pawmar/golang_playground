package fqueue

import (
    "testing"
)


func TestQueue(t *testing.T) {
    size := 5
    q := NewQueue(size)
    if !q.Empty() || q.Full() {
        t.Error("Newly initialized queue is not empty or full")
    }
    val, err := q.Dequeue()
    if err != nil {
        if val != 0 {
            t.Error("Newly initialized queue returns error but not 0 when dequeued")
        }
    } else {
        t.Error("Newly initialized queue should return error when dequeued")
    }
    results := make([]int, size)
    for i := 0; i < size; i++ {
        results[i] = i + 1
        q.Enqueue(results[i])
    }
    if !q.Full() {
        t.Error("Queue should be full")
    }
    for i := 0; i < size; i++ {
        result, _ := q.Dequeue()
        if result != results[i] {
            t.Error("Wrong value dequeued")
        }
    }
    if !q.Empty() {
        t.Error("Queue should be empty")
    }

    num := 1000000
    bigq := NewQueue(num)
    for i := 0; i < num; i++ {
        bigq.Enqueue(i)
    }
    if !bigq.Full() {
        t.Error("Big Queue should be full")
    }
    bigq.Clear()
    if !bigq.Empty() {
        t.Error("Biq Queue should be empty")
    }
    val, err = bigq.Dequeue()
    if err == nil {
        t.Log(val, err)
        t.Error("Cleared (thus empty) Queue shouldn't allow to dequeue")
    }
    for i := 0; i < num; i++ {
        bigq.Enqueue(i)
    }
    if !bigq.Full() {
        t.Error("Big Queue should be full")
    }

}