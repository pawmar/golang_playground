//Chess knight route
//Brute force solution using recursion

package main

import (
    "fmt"
)

const Size = 7
var board [Size][Size]int
var count = 0


func is_ok(x, y int) bool {
    return x >= 0 && x < Size &&
            y >= 0 && y < Size &&
            board[x][y] == 0
}

func jump(x, y int) bool {
    if count == Size*Size {
        return true
    }
    if is_ok(x, y) {
        count++
        board[x][y] = count

        if jump(x-2, y+1) || jump(x-2, y-1) ||
            jump(x+2, y+1) || jump(x+2, y-1) ||
            jump(x-1, y+2) || jump(x-1, y-2) ||
            jump(x+1, y+2) || jump(x+1, y-2) {
            return true
        } else {
            count--
            board[x][y] = 0
        }
    }
    return false
}

func print_board(board *[Size][Size]int) {
    for i := 0; i < len(board); i++ {
        for j := 0; j < len(board[i]); j++ {
            fmt.Printf("%02d ", board[i][j])
        }
        fmt.Printf("\n")
    }	
}


func main() {
    if jump(0, 0) {
        fmt.Println("Success!")
    } else {
        fmt.Println("Failure.")
    }
    print_board(&board)
}
